//Function: testbench for ISA circuit simulation, both RTL-level and gate-level.
//Input: RTL desciption circuit, config file to give clock period, stimuli data file. 
//Output: Outport_file recoding the output port signal at each cycle.
//Error_file, recoding the total timing error happens at each bit position. 
//Prerequisite: change the UUT name, change the port map in UUT declaration
//and top module declaration, and correspoding operand reading config. 
`timescale 1ns/1ps

module tb(output reg [31:0] X,
    output reg [31:0] Y,
    //output reg Cin,
    output [32:0] R_valid,
    output [32:0] R_ISA,
    output [32:0] R_ISA_PS,
    output [31:0] R_PL_valid,
    output reg clk, 
    output reg rst);
   
    
    reg [63:0] FF_error_cnt;
    integer N, L, M, K, test1, test2;
    parameter L4 = 32;
    integer struct_error_cnt [0:31];
    integer struct_timing_error_cnt [0:31];
    integer struct_err_yes, struct_timing_err_yes, struct_err_total = 0, struct_timing_err_total = 0; 
    integer line_number, comma_1, comma_2; 
    reg [20:0] cnt_cycle;
    reg Cin = 0;
    reg Cin_in;
    integer struct_error_file, struct_timing_error_file, outport_file, in_file, config_file, statusI, c_out;
    wire [31:0] c1, c2;
    reg [31:0] X_in, Y_in;
    reg [32:0] R_valid_out; 
    real  input_delay, capture_delay, half_period, clk_period;
    

    assign R_valid = X_in + Y_in + Cin_in; 
    ADDER32CLK UUT1(.clk(clk), .in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_ISA));
    ADDER32CLK_ps UUT2(.clk(clk), .in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_ISA_PS));
    //IntAdder_32_f400_uid2_Wrapper UUT2(.clk(clk), .rst(rst), .X(X), .Y(Y), .Cin(Cin), .R(R_valid));
    //IntAdder_32_f400_uid2_Wrapper_pl UUT3(.clk(clk), .rst(rst), .X(X), .Y(Y), .Cin(Cin), .R(R_PL_valid));
 
    initial
    begin       
       //outport_file = $fopen("../REP/outport.txt", "w");  
       struct_error_file = $fopen("../REP/struct_err.txt", "w");    //record bit level structure error
       struct_timing_error_file = $fopen("../REP/struct_time_err.txt", "w");  //record both structure_timing error
       config_file = $fopen("../sim/configure.txt","r");
       statusI = $fscanf(config_file,"%f",clk_period);
       $fclose(config_file);
       half_period=0.5*clk_period;
       $display("clock period is: %f", clk_period);
//       input_delay=0.2*half_period;
       capture_delay=0.2*half_period; 
     end

    initial
     begin
         clk = 0;
         forever  begin
                  #half_period  clk = !clk; //define the clock period  
         end
     end
         
    initial 
     begin
       M=32;
       in_file  = $fopen("../stimuli_data/stimuli.csv", "r");
       //in_file  = $fopen("../stimuli_data/stimuli_sample.csv", "r");
       rst=1; 
       cnt_cycle=0;
       test1=0;
       test2=0;
       for(N=0; N<32; N=N+1)
       begin
           struct_error_cnt[N]=0;
           struct_timing_error_cnt[N]=0;
       end
       #10 rst=0;
    end  
       
    always @(posedge clk)
    begin
       X_in = X;
       Y_in = Y;
       Cin_in = Cin;
       R_valid_out = R_valid; 
    end

    always @(negedge clk)
    begin
      #input_delay //the setup time for TSMC FF is 0.1ns.so 1.2-0.1*3>1.1ns
      //test1 = $fscanf(in_file, "%b\t%b\t%b\n", X, Y, Cin);
      test1 = $fscanf(in_file, "%d%c%b%c%b\n", line_number, comma_1, X, comma_2, Y); 
      //$display("%b\t%b\n",X,Y);
      if(test1!==5)
      begin
          for(L=0; L<32; L=L+1)
          begin
             $fdisplay(struct_error_file,"cycle %d:bit_error_cnt[%d]=%d",cnt_cycle,L,struct_error_cnt[L]);
             $fdisplay(struct_timing_error_file,"cycle %d:bit_error_cnt[%d]=%d",cnt_cycle,L,struct_timing_error_cnt[L]);
          end
          $fdisplay(struct_error_file,"total struct erroneous cycle is: %d",struct_err_total);
          $fdisplay(struct_timing_error_file,"total struct_timing erroneous cycle is: %d", struct_timing_err_total);
          $fclose(in_file);
          $finish; 
      end
      cnt_cycle=cnt_cycle+1;
    end

    always @(negedge clk)
    begin 
      #capture_delay
      struct_err_yes = 0; 
      struct_timing_err_yes = 0; 
/************ COMPARE DIAMOND, GOLDEN AND BRICK DATA **************/
      for(L=L4-1; L>=0; L=L-1) 
      begin
           if (R_ISA[L]!==R_valid_out[L])
           begin
               struct_err_yes = struct_err_yes + 1;
               struct_error_cnt[L] <= struct_error_cnt[L] + 1; 
           end
           if (R_ISA_PS[L]!==R_valid_out[L])
           begin
               struct_timing_err_yes = struct_timing_err_yes + 1; 
               struct_timing_error_cnt[L] <= struct_timing_error_cnt[L] + 1;
           end
      end

/***************** RECORD TOTAL ERRONEOUS CYCLE COUNT ******************/ 
      if (struct_err_yes > 0)
          struct_err_total = struct_err_total + 1;
      if (struct_timing_err_yes > 0)
          struct_timing_err_total = struct_timing_err_total + 1; 
    end      


endmodule


/****************** TRASH CODE ***************************/
/*
$fwrite(outport_file, "x");
$fwrite(outport_file, "\n"); 
*/

