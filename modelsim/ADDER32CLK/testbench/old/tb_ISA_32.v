`timescale 1ns/1ps
`define DELAY 20

/**************module declaration*****************/
module tb();
/*************************************************/
 
  parameter finishtime = 20; 

//----------------------------------------------------
//inputs to the DUT are reg type
  reg [31:0] a;
  reg [31:0] b;
  reg cin = 0; 

//----------------------------------------------------
//outputs from the DUT are wire type
  wire [32:0] s;
  //wire cout;
  wire [32:0] exact_s; 

/*****************define file operation parameter**************/
  integer read_operand; 
  integer stimuli_file;
  integer line_number, comma_1, comma_2; 


//----------------------------------------------------
//instantiate the Device Under Test (DUT)
//using named instantiation 
  ADDER32 DUT(.in_a(a), .in_b(b), .in_c(cin), .out_s(s));
  assign exact_s = a + b + cin; 

//---------------------------------------------------
//initialiaze all variables in a seperate block 
initial 
begin
    $display($time, " << Starting the simulation >>"); 
    //---------------------------------------------------
    //at time 0
    stimuli_file = $fopen("../stimuli_data/stimuli_sample.csv","r");
    read_operand = $fscanf(stimuli_file, "%d%c%b%c%b\n", line_number, comma_1, a, comma_2, b); 
    cin = 0; 
    //---------------------------------------------------
    //$display print once, $monitor print everytime any variable change
    $display ("-----------------------------------------");
    $display ("                                      ABS");
    $display ("-----------------------------------------");
    $monitor ("TIME = %d, a = %b, b = %b, cin = %b, s = %b, exact_s = %b",$time, a, b, cin, s, exact_s);
end

/*****************read input operands from stimuli file*********************/
always 
begin
    #`DELAY read_operand = $fscanf(stimuli_file, "%d%c%b%c%b\n", line_number, comma_1, a, comma_2, b); 
    if(read_operand!==5)
    begin
        $display ("Finishing simulation due to end of file.");
        $display ("Time is - %d", $time);
        $finish; 
    end
end

endmodule 

  
