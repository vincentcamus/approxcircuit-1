//Function: testbench for ISA circuit simulation, both RTL-level and gate-level.
//Input: post-syn netlist, SDF, clk    
//Output: diamond, golden, silver output
//Prerequisite: change the UUT name, change the port map in UUT declaration
//and top module declaration, and correspoding operand reading config. 
`timescale 1ns/1ps

module tb(output reg [31:0] X,
          output reg [31:0] Y,
          output [32:0] R_diamond_in,
          output [32:0] R_golden,
          output [32:0] R_silver,
          output reg clk);
   
    
    reg [63:0] FF_error_cnt;
    integer N, L, M, K, test1, test2;
    parameter L4 = 32;
    integer line_number, comma_1, comma_2; 
    reg Cin = 0;
    reg Cin_in;
    integer diamond_file, golden_file, silver_file; 
    integer in_file, config_file, statusI, c_out;
    wire [31:0] c1, c2;
    reg [31:0] X_in, Y_in;
    reg [32:0] R_diamond; 
    real  input_delay, capture_delay, half_period, clk_period;
    

    assign R_diamond_in = X_in + Y_in + Cin_in; 
    ADDER32CLK_ps UUT1(.clk(clk), .in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_golden));
    ADDER32CLK_ps UUT2(.clk(clk), .in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_silver));
 
    initial
    begin       
       diamond_file = $fopen("../REP/diamond.csv");
       golden_file = $fopen("../REP/golden.csv");
       silver_file = $fopen("../REP/silver.csv");
       config_file = $fopen("../sim/configure.txt","r");
       statusI = $fscanf(config_file,"%f",clk_period);
       half_period=0.5*clk_period;
       $display("clock period is: %f", clk_period);
//       input_delay=0.2*half_period;
       capture_delay=0.2*half_period; 
     end

    initial
     begin
         clk = 0;
         forever  begin
                  #half_period  clk = !clk; //define the clock period  
         end
     end
         
    initial 
     begin
       M=32;
       in_file  = $fopen("../stimuli_data/stimuli.csv", "r");
       test1=0;
       test2=0;
    end  
       
    always @(posedge clk)
    begin
       X_in = X;
       Y_in = Y;
       Cin_in = Cin;
       R_diamond = R_diamond_in; 
    end

    always @(negedge clk)
    begin
      #input_delay //the setup time for TSMC FF is 0.1ns.so 1.2-0.1*3>1.1ns
      //test1 = $fscanf(in_file, "%b\t%b\t%b\n", X, Y, Cin);
      test1 = $fscanf(in_file, "%d%c%b%c%b\n", line_number, comma_1, X, comma_2, Y); 
      //$display("%b\t%b\n",X,Y);
      if(test1!==5)
      begin
          $fclose(in_file);
          $finish; 
      end
    end

    always @(negedge clk)
    begin 
      #capture_delay
/************ RECORD DIAMOND, GOLDEN AND SILVER DATA **************/
      $fdisplay(diamond_file, "%b", R_diamond); 
      $fdisplay(golden_file, "%b", R_golden); 
      $fdisplay(silver_file, "%b", R_silver); 
    end      


endmodule

/****************** TRASH CODE ***************************/
/*
$fwrite(outport_file, "x");
$fwrite(outport_file, "\n"); 
*/

