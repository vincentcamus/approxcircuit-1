#Function: build prediction model for the bit-level timing error of ISA

import time
import struct
from random import * 
from collections import defaultdict
import numpy as np
import sklearn
import scipy
from sklearn import svm
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn import linear_model
from sklearn import neighbors
import glob

#create the binary vector as input feature
#normally we start with i = 1, this means  
def bin_feature(data_list,i):
    feat = []
    for j in range(2):
        for k in range(1,3):
            feat += map(int, data_list[i-1+j].strip().split(',')[k])
    return feat

def bin_sum_feature(data_list, sum_list, i, bit_position):
    feat = []
    for j in range(2):
        for k in range(1,3):
            feat += map(int, data_list[i-1+j].strip().split(',')[k])
        feat.append(int(sum_list[i+j].strip()[32-bit_position]))
    return feat


#create output label data 
def yFeature(TER_list,i,bit_position):
    bit_TER = map(int, TER_list[i].strip())[32-bit_position]
    return bit_TER

#calculate the prediction accuracy
def prediction_accuracy(X, y, clf): 
    hit = abs(clf.predict(X)-y)    #find the difference 
    accuracy = 1-float(sum(hit)) / len(hit)
    return accuracy   

def naive_predictor(test_y):
    hit = sum(test_y)
    accuracy = float(hit)/len(test_y)
    return accuracy
 
#return the files in a dir with pre and post constraints 
def find_filelist(pre_cons, post_cons):
   filename_list = [v[len(pre_cons):-len(post_cons)]  for v in glob.glob(pre_cons + "*" + post_cons)]
   return filename_list

   
#return the list of bit-level prediction accuracy 
def compute_bit_accuracy(design, clk):
    #initialize parameters 
    bit_accuracy = []
    bit_position_list = [v for v in range(32)] 
    train_data = 'stimuli_200k.csv'
    prediction_file = open('prediction_result','a')
    ISA = design + '.' + clk 

    ########################## Training phase ##########################
    #ISA = 'ISA_32_4_8888_x000_x000_xSSS_x000_x444_0.3-cu.0.255'
    #form training and testing input and output file 
    train_data_list = open('../stimuli_data/' + train_data,'r').readlines()
    train_sum_list = open('../REP/golden.' + ISA + '.csv', 'r').readlines()
    train_TER_list = open("../TER_REP/TER." + ISA + '.csv','r').readlines()
    test_data_list = open('../stimuli_data/' + train_data,'r').readlines()
    test_sum_list = open('../REP/golden.' + ISA + '.csv').readlines()
    test_TER_list = open('../TER_REP/TER.' + ISA + '.csv', 'r').readlines()
    TRAIN = int(len(train_data_list) * 0.8)   
    TEST = int(len(test_data_list) * 0.1)    

    #for each bit, train and test the model
    for bit_position in bit_position_list:
        #Extracing features
        #train_X1 = [bin_feature(train_data_list,i) for i in range(1, TRAIN)]  #start from the 2nd element of training data because it has preceding input  
        train_X2 = [bin_sum_feature(train_data_list,train_sum_list,i,bit_position) for i in range(1, TRAIN)] 
        #print "hehehe"
        train_y = [yFeature(train_TER_list,i,bit_position) for i in range(2, TRAIN+1)] #the 1st element map to 2nd element 

        #check whether all output belong to one class 
        if(sum(train_y) == len(train_y)):
            bit_accuracy.append(1)
            print  "skip " + str(bit_position)
            continue 

        #clf1 = linear_model.LogisticRegression()
        #clf1 = tree.DecisionTreeClassifier()
        clf1 = RandomForestClassifier(n_estimators=50)
        clf1.fit(train_X2, train_y)
        print "model fitting complete"

        #####################Testing phase ##############################
        #extracting features 
        #test_X1 = [bin_feature(test_data_list,i) for i in range(TRAIN,TRAIN+TEST)]
        test_X2 = [bin_sum_feature(test_data_list,test_sum_list,i,bit_position) for i in range(TRAIN,TRAIN+TEST)]
        test_y = [yFeature(test_TER_list,i,bit_position) for i in range(TRAIN+1,TRAIN+TEST+1)]

        start_time = time.time()
        ML_accuracy = prediction_accuracy(test_X2, test_y, clf1)
        bit_accuracy.append(ML_accuracy)
        print("--- %s seconds ---" % (time.time() - start_time))
        prediction_file.write("\n--- " + str(time.time() - start_time) + " seconds ---\n")
        print "Prediction accuracy for clf1 is ", ML_accuracy
        prediction_file.write("Prediction accuracy for clf1 is " +  str(ML_accuracy) + ' for ' + '\n')
        prediction_file.write(str(clf1)+'\n')

        #################### Naive Model ###################################### 
        naive_accuracy = naive_predictor(test_y)
        print "Prediction accuracy for naive is ", naive_accuracy
        prediction_file.write("Prediction accuracy for naive is " + str(naive_accuracy) + ' for ' +'\n')
    open('../stimuli_data/' + train_data,'r').close()
    open('../REP/golden.' + ISA + '.csv', 'r').close()
    open("../TER_REP/TER." + ISA + '.csv','r').close()
    open('../stimuli_data/' + train_data,'r').close()
    open('../REP/golden.' + ISA + '.csv').close()
    open('../TER_REP/TER.' + ISA + '.csv', 'r').close()
    return bit_accuracy 


#define parameters 
pre_cons = '../REP/diamond.'

for clk in ['0.255', '0.27', '0.285']:
#for clk in ['0.255']:
    post_cons = '.' + clk + '.csv'
    for design in find_filelist(pre_cons, post_cons):
    #for design in ['ISA_32_4_8888_x000_x000_xSSS_x111_x666_0.3-cu']:
        bit_accuracy = compute_bit_accuracy(design, clk)
        open('prediction_accuracy', 'a').write(clk + '\t' + design + '\t' + str(float(sum(bit_accuracy))/len(bit_accuracy)) + '\t' + str(bit_accuracy) + '\n')









######################## TRASH CODE ##################################
'''x1 = map(int, data_list[i-1].split()[0][2:])
x2 = map(int, data_list[i-1].split()[1][2:])
x3 = map(int, data_list[i].split()[0][2:])
x4 = map(int, data_list[i].split()[1][2:])
feat = feat + x1 + x2 + x3 + x4'''
#feat = feat +  x3 + x4
'''def bin_sum_feature(data_list,sum_list,i):
    global taken
    feat = []
    x1 = map(int, data_list[i-1].split()[0])
    x2 = map(int, data_list[i-1].split()[1])
    x3 = map(int, data_list[i].split()[0])
    x4 = map(int, data_list[i].split()[1])
    sum1 = map(int, sum_list[i-1+2][2:].strip())
    sum2 = map(int, sum_list[i+2][2:].strip())
    feat = feat + x1 + x2 + x3 + x4 + sum1 + sum2
    #feat = feat + x3 + x4 + sum2
    return feat

def sum_feature(op1, op2, bit_width):
    op1 = int(op1,2)
    op2 = int(op2,2)
    op1 = struct.unpack("f",struct.pack("I", op1))[0]
    op2 = struct.unpack("f",struct.pack("I", op1))[0]
    result = op1 + op2
    result = struct.pack('f',result)
    result = struct.unpack('I',result)
    result = format(int(bin(result[0]),2),'032b') 
    return result'''
    #hit = abs(np.true_divide(clf.predict(X),threshold).astype(int) - np.true_divide(y, threshold).astype(int))<1
    #hit = int(np.true_divide(np.asarray(clf.predict(X)), 300)) == int(np.true_divide(np.asarray(y), 300))    #using exactly same class as match

#print "Prediction accuracy for rand is ", rand_accuracy
#print "Prediction accuracy for naive is ", naive_accuracy
#print "MSE is", MSE_ran(test_X, test_y, clf)
#prediction_file.write("Accuracy of clf classifier is " + str(ML_accuracy) + ' for ' + test_data + '\n')
#prediction_file.write("Accuracy of rand classifier is " + str(rand_accuracy) + ' for ' + test_data + '\n')
#prediction_file.write("Accuracy of naive classifer is " + str(naive_accuracy) + ' for ' + test_data + '\n')
#rand_accuracy = rand_predictor(train_y, test_y)
#naive_accuracy = naive_predictor(train_y, test_y)
#clf = RandomForestRegressor(n_estimators=100, max_features='log2')
#clf = RandomForestRegressor(n_estimators=100)
#clf = linear_model.LinearRegression()
#clf = neighbors.KNeighborsRegressor()
#clf = linear_model.SGDRegressor()
#clf = tree.DecisionTreeClassifier()
#clf = RandomForestClassifier()
#clf = svm.SVC()
