#Function: plot the joint effects of all three types of errors 
from collections import defaultdict 
import glob
import matplotlib.pyplot as plt 
import numpy as np

#return an average structural error between diamond list and golden list  
def compute_ave_struct_error(ISA, clk):
   diamond_list = open('../REP/diamond.' + ISA + '.' + clk + '.csv', 'r').readlines()
   golden_list = open('../REP/golden.' + ISA + '.' + clk + '.csv', 'r').readlines()
   ave_rela_error = 0 
   for i in range(1,len(diamond_list)):
      diamond_value = int(diamond_list[i].strip(), 2) 
      golden_value = int(golden_list[i].strip(), 2)
      new_rela_error = float((golden_value-diamond_value))/diamond_value 
      ave_rela_error = (ave_rela_error * (i-1) + new_rela_error)/float(i)
   open('../REP/diamond.' + ISA + '.' + clk + '.csv', 'r').close()
   open('../REP/golden.' + ISA + '.' + clk + '.csv', 'r').close()
   return ave_rela_error

#check whether a string is a number 
def check_number(value):
   try:
      int_value = int(value,2)
      return int_value 
   except ValueError: 
      return False 

#return the corrected silver value 
def correct_silver_value(golden_binary, silver_binary):
   correct_binary = ''
   for i, bit in enumerate(silver_binary):
      if bit == 'x':
         correct_binary += str(abs(int(golden_binary[i])-1))
      else: 
         correct_binary += bit
   correct_silver_value = int(correct_binary, 2) 
   #print golden_binary, silver_binary, correct_binary
   return correct_silver_value 

#return average struct error, timing error and joint error 
def compute_ave_error(ISA, clk):
   diamond_list = open('../REP/diamond.' + ISA + '.' + clk + '.csv', 'r').readlines()
   golden_list = open('../REP/golden.' + ISA + '.' + clk + '.csv', 'r').readlines()
   silver_list = open('../REP/silver.' + ISA + '.' + clk + '.csv', 'r').readlines()
   ave_rela_struct_error = 0 
   ave_rela_timing_error = 0 
   ave_rela_joint_error = 0 
   for i in range(1,len(golden_list)):
      diamond_value = int(diamond_list[i].strip(), 2) 
      golden_value = int(golden_list[i].strip(),2)
      silver_value = check_number(silver_list[i].strip()) 
      if(silver_value == False):
         silver_value = correct_silver_value(golden_list[i].strip(), silver_list[i].strip())
      #silver_value = int(silver_list[i].strip(),2)    
      new_rela_struct_error = float((golden_value-diamond_value))/diamond_value 
      ave_rela_struct_error = (ave_rela_struct_error * (i-1) + abs(new_rela_struct_error))/float(i) #use abs for mean value 
      new_rela_timing_error = float(silver_value - golden_value)/diamond_value 
      ave_rela_timing_error = (ave_rela_timing_error * (i-1) + abs(new_rela_timing_error))/float(i)
      new_rela_joint_error = float(silver_value - diamond_value)/diamond_value 
      ave_rela_joint_error = (ave_rela_joint_error * (i-1) + abs(new_rela_joint_error))/float(i)
   open('../REP/diamond.' + ISA + '.' + clk + '.csv', 'r').close()
   open('../REP/golden.' + ISA + '.' + clk + '.csv', 'r').close()
   open('../REP/silver.' + ISA + '.' + clk + '.csv', 'r').close()
   print ave_rela_timing_error
   return ave_rela_struct_error, ave_rela_timing_error, ave_rela_joint_error 

#return the files in a dir with pre and post constraints 
def find_filelist(pre_cons, post_cons):
   filename_list = [v[len(pre_cons):-len(post_cons)]  for v in glob.glob(pre_cons + "*" + post_cons)]
   return filename_list

#define parameters 
pre_cons = '../REP/diamond.'
post_cons = '.0.255.csv'
three_errors = defaultdict(list) 
struct_list = [] 
timing_list = []
joint_list = []
ISA_list = find_filelist(pre_cons, post_cons)
print ISA_list

#for clk in ['0.255', '0.27', '0.285']:
for clk in ['0.255']:
   post_cons = '.' + clk + '.csv' 
   for ISA in ISA_list:
        ave_rela_struct_error, ave_rela_timing_error, ave_rela_joint_error = compute_ave_error(ISA, clk)
        struct_list.append(ave_rela_struct_error)
        timing_list.append(ave_rela_timing_error)
        joint_list.append(ave_rela_joint_error)
        #three_errors[(ISA, clk)].extend(compute_ave_error(ISA, clk))
#print struct_list

#draw 2D line 
color_list = ['ro-','go-','bo-']
x_list = [v+1 for v in range(13)]
error_list = ['struct_error', 'timing_error', 'joint_error']
plt.plot(x_list, struct_list, color_list[0], label=error_list[0])
plt.plot(x_list, timing_list, color_list[1], label=error_list[1])
plt.plot(x_list, joint_list, color_list[2], label=error_list[2])
#plt.yscale('log')    # if we want log style
plt.xticks(np.arange(min(x_list), max(x_list)+1))
#plt.xticks(np.arange(min(x_list), max(x_list)+1), rotation=45)
plt.xlabel('ISA Design')
plt.ylabel('Average Relative Arithmetic Error')
plt.legend(bbox_to_anchor=(0.91, 0.9), bbox_transform=plt.gcf().transFigure)
plt.show()




