-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- ISA TOP ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ISA IS
	GENERIC (
		ADDER_WIDTH : INTEGER; -- total bit-width of the ISA
		BLOCK_NB    : INTEGER; -- number of blocks of the ISA
		ADD_REG     : STRING;  -- ADD sizes                      char sizes: 0-9,a-z,A-Y=0..60 and Z=64 !!!
		SPEC_REG    : STRING;  -- SPEC sizes                     char sizes: 0-9,a-z,A-Y=0..60
		GUESS_REG   : STRING;  -- SPEC guesses                   char types: 0,1,A,B,G,P,X,C,-
		TEST_REG    : STRING;  -- COMP types                     char types: S=standard,N=no-test
		COR_REG     : STRING;  -- COMP correction sizes          char sizes: 0-9,a-z,A-Y=0..60
		RED_REG     : STRING); -- COMP reduction/balancing sizes char sizes: 0-9,a-z,A-Y=0..60
	PORT (
		a, b   : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cin    : IN  STD_LOGIC;
		s      : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cout   : OUT STD_LOGIC);
END ENTITY ISA;

------------------------------------------------------------------- ISA TOP ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF ISA IS

---------------------------------- COMPONENTS

	-- stage 1 - SPEC stage
	COMPONENT SPEC_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING;
		SPEC_REG    : STRING;
		GUESS_REG   : STRING);
	PORT (
		a       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		b       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cin     : IN  STD_LOGIC;
		c_addb  : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		c_specb : OUT STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0));
	END COMPONENT;

	-- stage 2 - ADD stage
	COMPONENT ADD_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING);
	PORT (
		a       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		b       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_specb : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		s_addb  : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_addb  : OUT STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0));
	END COMPONENT;

	-- stage 3 - COMP stage
	COMPONENT COMP_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING;
		GUESS_REG   : STRING;
		TEST_REG    : STRING;
		COR_REG     : STRING;
		RED_REG     : STRING);
	PORT (
		s_addb  : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_specb : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		c_addb  : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		s_compb : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_compb : OUT STD_LOGIC);
	END COMPONENT;
	
---------------------------------- SIGNALS

	SIGNAL c_specb : STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0); -- stage 1 output
	SIGNAL c_addb  : STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0); -- stage 2 output
	SIGNAL s_addb  : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);  -- stage 2 output
	SIGNAL s_compb : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);  -- stage 3 output

---------------------------------- DESCRIPTION
	
BEGIN

	-- stage 1 - SPEC stage
	inst_SPEC_stage: SPEC_stage
	GENERIC MAP (ADDER_WIDTH, BLOCK_NB, ADD_REG, SPEC_REG, GUESS_REG)
	PORT MAP (a, b, cin, c_addb, c_specb);

	-- stage 2 - ADD stage
	inst_ADD_stage: ADD_stage
	GENERIC MAP (ADDER_WIDTH, BLOCK_NB, ADD_REG)
	PORT MAP (a, b, c_specb, s_addb, c_addb);
	
	-- stage 3 - COMP stage
	inst_COMP_stage: COMP_stage
	GENERIC MAP (ADDER_WIDTH, BLOCK_NB, ADD_REG, GUESS_REG, TEST_REG, COR_REG, RED_REG)
	PORT MAP (s_addb, c_specb, c_addb, s, cout);

END ARCHITECTURE standard;
