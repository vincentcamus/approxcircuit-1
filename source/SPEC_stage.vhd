-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- STAGE 1 TOP ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.functions_pkg.ALL;

ENTITY SPEC_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING;
		SPEC_REG    : STRING;
		GUESS_REG   : STRING);
	PORT (
		a       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		b       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cin     : IN  STD_LOGIC;
		c_addb  : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		c_specb : OUT STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0));
END ENTITY SPEC_stage;

------------------------------------------------------------------- STAGE 1 TOP ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF SPEC_stage IS

---------------------------------- COMPONENTS

	-- component SPEC declaration
	COMPONENT SPEC IS
		GENERIC (
			SPEC_WIDTH : INTEGER);
		PORT (
			ai : IN  STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);
			bi : IN  STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);
			ci : IN  STD_LOGIC;
			co : OUT STD_LOGIC);
	END COMPONENT;

---------------------------------- SIGNALS

	SIGNAL ci : STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 1);

---------------------------------- DESCRIPTION

BEGIN

	-- SPEC block instantiations
	gen_SPEC: FOR i IN BLOCK_NB-1 DOWNTO 1 GENERATE
		nolink: IF GUESS_REG(i+1) /= '-' GENERATE
			-- SPEC
			inst_SPEC: SPEC
			GENERIC MAP (
				char_to_int(SPEC_REG(i+1)))
			PORT MAP (
				a(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))),
				b(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))),
				ci(i),
				c_specb(i));
		END GENERATE;
	END GENERATE;

	-- various guess input generations
	gen_GUESS: FOR i IN BLOCK_NB-1 DOWNTO 1 GENERATE
		-- fixed guess '0'
		guess_0: IF GUESS_REG(i+1) = '0' GENERATE
			ci(i) <= '0';
		END GENERATE;
		-- fixed guess '1'
		guess_1: IF GUESS_REG(i+1) = '1' GENERATE
			ci(i) <= '1';
		END GENERATE;
		-- guessed at operand A input 
		guess_A: IF GUESS_REG(i+1) = 'A' GENERATE
			ci(i) <= a(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1);
		END GENERATE;
		-- guessed at operand B input
		guess_B: IF GUESS_REG(i+1) = 'B' GENERATE
			ci(i) <= b(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1);
		END GENERATE;
		-- guessed at operands' generate signal
		guess_G: IF GUESS_REG(i+1) = 'G' GENERATE
			ci(i) <= a(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1)
				AND b(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1);
		END GENERATE;
		-- guessed at operands' propagate signal, defined with a OR !!!
		guess_P: IF GUESS_REG(i+1) = 'P' GENERATE
			ci(i) <= a(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1)
				OR b(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1);
		END GENERATE;
		-- guessed at operands' propagate signal, defined with a XOR !!!
		guess_X: IF GUESS_REG(i+1) = 'X' GENERATE
			ci(i) <= a(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1)
				XOR b(block_start(ADD_REG, i)-char_to_int(SPEC_REG(i+1))-1);
		END GENERATE;
		-- guessed at ISA carry-in
		guess_C: IF GUESS_REG(i+1) = 'C' GENERATE
			ci(i) <= cin;
		END GENERATE;
		-- no guess, linked to previous block
		guess_link: IF GUESS_REG(i+1) = '-' GENERATE
			c_specb(i) <= c_addb(i-1);
		END GENERATE;
	END GENERATE;

	-- guess boundary
	c_specb(0) <= cin;

END ARCHITECTURE standard;

------------------------------------------------------------------- STAGE 1 SPEC ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY SPEC IS
	GENERIC (
		SPEC_WIDTH : INTEGER);
	PORT (
		ai : IN  STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);
		bi : IN  STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);
		ci : IN  STD_LOGIC;
		co : OUT STD_LOGIC);
END ENTITY SPEC;

------------------------------------------------------------------- STAGE 1 SPEC ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF SPEC IS

---------------------------------- SIGNALS

	SIGNAL c : STD_LOGIC_VECTOR (SPEC_WIDTH DOWNTO 0);
	SIGNAL p : STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);
	SIGNAL g : STD_LOGIC_VECTOR (SPEC_WIDTH-1 DOWNTO 0);

---------------------------------- DESCRIPTION

BEGIN

	-- CLA pre-processing: initial P/G generation
	g <= ai AND bi;
	p <= ai XOR bi;

	-- carries
	gen_carries: FOR i IN SPEC_WIDTH DOWNTO 1 GENERATE
		c(i) <= g(i-1) OR ( c(i-1) AND p(i-1) );
	END GENERATE;
	
	-- boundaries
	c(0) <= ci;
	co <= c(SPEC_WIDTH);

END ARCHITECTURE standard;
